<?php 
// ============================================================================
// Theme support section 
// ============================================================================
    //Adds dynamic title tag support
    function personal_blog_theme_support(){
        add_theme_support('title_tag');
        add_theme_support('custom-log');
    }
    
    add_action('after_theme_setup','personal_blog_theme_support');

// ===========================================================================
// supporting menu for sidebar 
// ===========================================================================
function personal_blog_menus(){
    // since theme has separate menus for mobile and descktop, first check the location
    $locations=array(
        'primary' => "Descktop Primary Left-sidebar",
        'footer' => "Footer Menu Items"
    );
    // register the different menus for admin pannel (in "Appearance > Menus")
    register_nav_menus($locations);
}

add_action('init', 'personal_blog_menus');


// ============================================================================
// loading styling and script files
// ============================================================================
    function personal_styling_finder_register(){
        $version=wp_get_theme()->get('Version');
        wp_enqueue_style('personalBlog-style', get_template_directory_uri().'/style.css', array(), $version,'all');
        wp_enqueue_style('personalBlog-bootstrap', "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css", array(), '4.4.1','all');
        wp_enqueue_style('personalBlog-fontawesome', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css", array(), '5.13.0','all');
    }
    // runn this function while wp_enqueue hoock is fired
    add_action('wp_enqueue_scripts','personal_styling_finder_register');

    function personal_syling_finder_scripts(){
       wp_enqueue_script('PersonalBlog-jquery','https://code.jquery.com/jquery-3.4.1.slim.min.js',array(),'3.4.1',true);
       wp_enqueue_script('PersonalBlog-popper','https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js',array(),'3.4.1',true);
       wp_enqueue_script('PersonalBlog-bootstrap','https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js',array(),'4.4.1',true);
       wp_enqueue_script('PersonalBlog-javascript',get_template_directory_uri().'/assets/js/main.js',array(),'1.0',true);
    }
    add_action('wp_enqueue_scripts','personal_syling_finder_scripts');
?>